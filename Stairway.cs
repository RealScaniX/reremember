﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stairway : MonoBehaviour {

    [SerializeField]
    private Vector2 groundOffset = new Vector2(0, 0.5f);
    [SerializeField]
    private Vector2 belowOffset = new Vector2(0, -0.5f);

    private BoxCollider2D colly;
    private Transform tx;
    private float passthroughStart;

    void Awake() {
        colly = GetComponent<BoxCollider2D>();
        tx = transform;
    }

	void FixedUpdate() {
		Vector2 up = tx.up;
        Vector2 pos = tx.position;
        // Switch on if above
        Vector2 playerFeetPos = GameControl.instance.GetPlayer().feetPosition;
        if (Time.time - passthroughStart > 0.75f) {
            Vector2 playerDelta = playerFeetPos - (pos + up * groundOffset);
            if (Vector2.Angle(up, playerDelta) < 90) {
                if (!colly.enabled) {
                    colly.enabled = true;
                    GameControl.instance.GetPlayer().WakeUp();
                }
            }
        }
        // Switch off if below
        {
            Vector2 playerDelta = playerFeetPos - (pos + up * belowOffset);
            if (Vector2.Angle(-up, playerDelta) < 90) {
                passthroughStart = -1;
                if (colly.enabled) {
                    colly.enabled = false;
                    GameControl.instance.GetPlayer().WakeUp();
                }
            }
        }
	}

    internal void Passthrough() {
        passthroughStart = Time.unscaledTime;
        colly.enabled = false;
        GameControl.instance.GetPlayer().WakeUp();
    }
}
