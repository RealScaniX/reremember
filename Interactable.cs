﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour {

    [SerializeField]
    public Vector3 keyHintOffset = new Vector3(0, -0.5f, 0);

    internal GameMode actionProcessor;

    private bool _interactive = true;
    private float triggerTime;

    public bool interactive {
        get {
            return _interactive;
        }
        set {
            this._interactive = value;
            if (!interactive) {
                if (GameControl.instance.focusedInteractable == this) {
                    GameControl.instance.focusedInteractable = null;
                    GameControl.instance.HideHint();
                }
            }
        }
    }

    public virtual void Trigger() {
        if (!interactive)
            return;
        if (CoolingDown())
            return;
        triggerTime = Time.time;
        if (actionProcessor != null)
            GameControl.instance.PushGameMode(actionProcessor);
    }

    public bool CoolingDown() {
        return Time.time - triggerTime < 2;
    }

    protected virtual void OnTriggerEnter2D(Collider2D other) {
        if (!interactive)
            return;
        GameControl.instance.focusedInteractable = this;
        GameControl.instance.ShowHint("[E] / [Enter]", transform.position + keyHintOffset);
    }

    protected virtual void OnTriggerExit2D(Collider2D other) {
        if (!interactive)
            return;
        if (GameControl.instance.focusedInteractable == this) {
            GameControl.instance.focusedInteractable = null;
            GameControl.instance.HideHint();
        }
    }
}
