using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class YouDedGameMode : GameMode {
    private readonly string[] messages;

    public YouDedGameMode(params string[] messages) {
        this.messages = messages;
    }

    public void EnterMode(bool push) {
        for (int i=0; i<messages.Length; i++)
            GameControl.instance.AddMessage(messages[i], GameControl.MESSAGECOLOR_INFO, GameControl.SpeechType.SYSTEM_SILENT, i == 0);
        GameControl.instance.GetPlayer().StopBody();
    }

    public void FixedUpdateMode() {
    }

    public void LeaveMode(bool pop) {
    }

    public void UpdateMode() {
        if (!GameControl.instance.HasPendingMessages()) {
            GameControl.instance.PopGameMode();

            GameControl.instance.SetCameraTile(0, 0);
            GameControl.instance.GetPlayer().ResetPosition();
        }
    }
}

