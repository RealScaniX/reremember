public interface GameMode {
    void EnterMode(bool push);

    void LeaveMode(bool pop);

    void UpdateMode();

    void FixedUpdateMode();
}