using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ElevatorTransition : TransitionGameMode {

    [SerializeField]
    private ElevatorTransition[] elevatorLevels;

    private float position;
    private DoorTransition doorTransition;
    private int myLevel;
    private ElevatorTransition targetElevator;
    private int targetLevel;
    private PlayerController player;
    private float startPosition;
    private bool playedPing;

    void Awake() {
        doorTransition = GetComponent<DoorTransition>();
        myLevel = 0;
        for (int i=0; i<elevatorLevels.Length; i++) {
            if (elevatorLevels[i] == this)
                myLevel = i+1;
        }
    }

    public void MoveToLevel(int level) {
        if (level == myLevel)
            return;
        targetElevator = elevatorLevels[level-1];
        targetLevel = level;
        doorTransition.canceled = true;
        GameControl.instance.PushGameMode(this);
    }

    internal override void FinishTransition() {
        if (transitionForward) {
            doorTransition.Apply(0);
            targetElevator.doorTransition.Apply(0);

            SetPlayerZ(0f);
         }
    }

    internal override void PlayTransition(float f) {
        if (!transitionForward)
            return;
        float t = f * duration;
        if (t < doorTransition.duration) {
            SetPlayerZ(0.15f);
            // Close door
            f = t / doorTransition.duration;
            doorTransition.Apply(1-f);
        } else if (t <= duration - 2*doorTransition.duration) {
            SetPlayerZ(15f);
            doorTransition.Apply(0); // make sure to shut door for stuttery computers
            // Moving
            f = (t - doorTransition.duration) / (duration - 3*doorTransition.duration);
            Vector2 bodyPosition = player.position;
            bodyPosition.y = Mathf.Lerp(startPosition, position, Mathf.SmoothStep(0, 1, f));
            player.position = bodyPosition;
        } else if (duration - t > doorTransition.duration) {
            SetPlayerZ(0.15f);
            doorTransition.Apply(0); // make sure to shut door for stuttery computers
            if (!playedPing) {
                playedPing = true;
                OneShotAudio.PlayClip(GameControl.instance.soundPing, Vector3.zero, 0f, 0.7f);
            }
            // Open door
            f = Mathf.Min(1, (t - (duration - 2*doorTransition.duration)) / doorTransition.duration);
            targetElevator.doorTransition.Apply(f);
        } else {
            playedPing = false;
            SetPlayerZ(0f);
            doorTransition.Apply(0); // make sure to shut door for stuttery computers
            // Close door
            f = Mathf.Min(1, (duration - t) / doorTransition.duration);
            targetElevator.doorTransition.Apply(f);
        }
    }

    private void SetPlayerZ(float z) {
        Vector3 pos = GameControl.instance.GetPlayer().transform.position;
        pos.z = z;
        player.transform.position = pos;
    }

    internal override void PrepareTransition() {
        if (transitionForward) {
            duration =
                doorTransition.duration +
                Mathf.Abs(myLevel - targetLevel) * 1.25f +
                doorTransition.duration +
                doorTransition.duration;
            player = GameControl.instance.GetPlayer();
            startPosition = GameControl.instance.GetPlayer().position.y;
            position = startPosition + (targetLevel - myLevel) * 6;
            SetPlayerZ(0.15f);
        }
    }
}