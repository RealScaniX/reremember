using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneShotAudio : MonoBehaviour {

    public static AudioSource PlayClip(AudioClip clip, Vector3 pos, float threeDness, float volume = 1f, float pitch = 1f, float delay = 0) {
        GameObject o = new GameObject("OneShotAudio." + clip.name);

        AudioSource audioSource = o.AddComponent<AudioSource>();
        audioSource.playOnAwake = false;
        audioSource.clip = clip;
        audioSource.bypassEffects = true;
        audioSource.bypassListenerEffects = true;
        audioSource.bypassReverbZones = true;
        audioSource.volume = GameControl.volSound * volume;
        audioSource.spatialBlend = threeDness;
        audioSource.pitch = pitch;

        OneShotAudio oneShot = o.AddComponent<OneShotAudio>();
        oneShot.audioSource = audioSource;
        oneShot.delay = delay;

        return audioSource;
    }

    private AudioSource audioSource;
    private float delay;
    private bool started;

    void Start() {
        if (delay > 0)
            audioSource.PlayDelayed(delay);
        else
            audioSource.Play();
        started = true;
    }

    void Update() {
        if (started && !audioSource.isPlaying) {
            audioSource.Stop();
            Destroy(gameObject);
        }
    }

}
