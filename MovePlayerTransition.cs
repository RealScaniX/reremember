using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class MovePlayerTransition : TransitionGameMode {

    internal float position;
    private Rigidbody2D body;
    private float startPosition;

    internal override void FinishTransition() {
        if (transitionForward) {
            GameControl.instance.GetPlayer().SetFaceDirection((position - gameObject.transform.position.x) < 0 ? true : false);
            GameControl.instance.GetPlayer().WakeUp();
        }
    }

    internal override void PlayTransition(float f) {
        if (!transitionForward)
            return;
        Vector2 bodyPosition = body.position;
        bodyPosition.x = Mathf.Lerp(startPosition, position, Mathf.SmoothStep(0, 1, f));
        body.MovePosition(bodyPosition);
    }

    internal override void PrepareTransition() {
        if (transitionForward) {
            body = GameControl.instance.GetPlayer().body;
            GameControl.instance.GetPlayer().StopBody();
            startPosition = GameControl.instance.GetPlayer().position.x;
            float delta = Mathf.Abs(startPosition - position);
            duration = 0.1f + delta / 3;
            if (delta > 0.05f)
                GameControl.instance.GetPlayer().SetFaceDirection((position - startPosition) > 0 ? true : false);
        } else {
            duration = 0.0001f;
        }
    }
}