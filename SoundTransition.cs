using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class SoundTransition : TransitionGameMode {

    [Header("SoundTransition")]
    [SerializeField]
    private AudioClip sound;
    private GameObject tmp;

    internal override void FinishTransition() {
    }

    internal override void PlayTransition(float f) {
    }

    internal override void PrepareTransition() {
        if (transitionForward) {
            duration = sound.length;
            OneShotAudio.PlayClip(sound, gameObject.transform.position, 0f);
        } else {
            duration = 0.01f;
        }
    }
}