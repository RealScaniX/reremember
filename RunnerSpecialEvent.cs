﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunnerSpecialEvent : SpecialEvent {

    [SerializeField]
    internal TalkInteractable otherDoor;

    [SerializeField]
    internal TalkInteractable otherDoorEmpty;

    [SerializeField]
    private float runDuration = 3f;

    private static float runStartTime = -1;


    public override void Trigger() {
        if (runDuration <= 0) {
            runStartTime = -1; // stop other one
        } else {
            GameControl.instance.AddMessage("Press and hold [SHIFT] now!", GameControl.MESSAGECOLOR_INFO, GameControl.SpeechType.SYSTEM_SILENT, true, null);
            runStartTime = Time.time;
            otherDoor.gameObject.SetActive(true);
            otherDoorEmpty.gameObject.SetActive(false);
        }
    }

    public override void DeTrigger() {
        if (runDuration <= 0) {
            otherDoor.gameObject.SetActive(false);
            otherDoorEmpty.gameObject.SetActive(true);
        }
    }

    void Update() {
        if (runDuration <= 0 || otherDoor == null)
            return;
        if (runStartTime > -1) {
            if (Time.time - runStartTime > runDuration) {
                GameControl.instance.AddMessage("Tooooo slooooow!", GameControl.MESSAGECOLOR_NPC, GameControl.SpeechType.NORMAL, true, null);
                runStartTime = -1;
                otherDoor.gameObject.SetActive(false);
                otherDoorEmpty.gameObject.SetActive(true);
            }
        }
    }
}
