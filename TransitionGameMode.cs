using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public abstract class TransitionGameMode : MonoBehaviour, GameMode {

    [SerializeField]
    internal float duration = 1.0f;

    internal GameMode destination;

    protected bool transitionForward;

    private float startTime;

    [HideInInspector]
    public bool canceled;

    public void EnterMode(bool push) {
        if (!push && canceled)
            return;
        canceled = false;
        if (push) {
            transitionForward = true;
        } else {
            transitionForward = false;
        }
        PrepareTransition();
        startTime = Time.time;
    }

    public void FixedUpdateMode() {
    }

    public void LeaveMode(bool pop) {
        if (canceled)
            return;
        if (!pop) {
            FinishTransition();
        }
    }

    public void UpdateMode() {
        float f = Mathf.Min(1, (Time.time - startTime) / duration);
        PlayTransition(f);
        if (f == 1) {
            if (transitionForward) {
                if (destination != null)
                    GameControl.instance.PushGameMode(destination);
                else
                    GameControl.instance.PopGameMode();
            } else {
                GameControl.instance.PopGameMode();
            }
        }
    }

    internal abstract void PrepareTransition();

    internal abstract void FinishTransition();

    internal abstract void PlayTransition(float f);
}