﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameControl : MonoBehaviour {
    public static bool BLOCK_ACTION_WHILE_MESSAGE_IS_VISIBLE = false;

    public static readonly KeyCode[] UP_KEYS = {KeyCode.UpArrow, KeyCode.W};
    public static readonly KeyCode[] DOWN_KEYS = {KeyCode.DownArrow, KeyCode.S};
    public static readonly KeyCode[] JUMP_KEYS = {KeyCode.Space, KeyCode.UpArrow, KeyCode.W};
    public static readonly KeyCode[] LEFT_KEYS = {KeyCode.A, KeyCode.LeftArrow};
    public static readonly KeyCode[] RIGHT_KEYS = {KeyCode.D, KeyCode.RightArrow};
    public static readonly KeyCode[] ACTIVATE_KEYS = {KeyCode.E, KeyCode.Return, KeyCode.KeypadEnter};
    public static readonly KeyCode[] MENU_KEYS = {KeyCode.Escape, KeyCode.F1};
    public static readonly KeyCode[] SKIP_MESSAGE_KEYS = {KeyCode.LeftShift, KeyCode.RightShift};

    public static readonly KeyCode[] RUN_KEYS = {KeyCode.LeftShift, KeyCode.RightShift};

    public const int LAYER_INTERACTIVES = 8;
    public const int LAYER_PLAYER = 9;
    public const int LAYER_FLOOR = 10;
    public const int LAYERMASK_PLAYER = 1 << LAYER_PLAYER;
    public const int LAYERMASK_INTERACTIVES = 1 << LAYER_INTERACTIVES;
    public const int LAYERMASK_FLOOR = 1 << LAYER_FLOOR;

    public static readonly Color COLOR_INVISIBLE = new Color(0, 0, 0, 0);
    public static readonly Color COLOR_INVISIBLE_WHITE = new Color(1, 1, 1, 0);

    public static readonly Color MESSAGECOLOR_PLAYER = Color.white;
    public static readonly Color MESSAGECOLOR_NPC = new Color(0.7f, 0.9f, 1.0f);
    public static readonly Color MESSAGECOLOR_INFO = new Color(1.0f, 0.95f, 0.6f);

    public enum SpeechType {
        NONE,
        NORMAL,
        QUESTION,
        IRRITATED,
        MOTIVATING,
        SYSTEM_SILENT,
        SYSTEM_REMEMBER,
        SYSTEM_FORGET
    }

    public static bool gameStarted;
    public static int musicVolume = 3;
    public static int soundVolume = 5;

    [SerializeField]
    internal AudioClip soundRemember;
    [SerializeField]
    internal AudioClip soundForget;
    [SerializeField]
    internal AudioClip soundMenuSelect;
    [SerializeField]
    internal AudioClip soundMenuActivate;
    [SerializeField]
    internal AudioClip soundPing;

    [SerializeField]
    private AudioClip[] normalVoices;
    [SerializeField]
    private AudioClip[] questionVoices;
    [SerializeField]
    private AudioClip[] irritatedVoices;
    [SerializeField]
    private AudioClip[] motivatingVoices;

    [SerializeField]
    private GameObject keyHint;

    [SerializeField]
    private GameObject messagePrefab;
    [SerializeField]
    private GameObject pendingMessageDisplay;
    [SerializeField]
    internal GameObject dialogOptionPrefab;
    [SerializeField]
    private GameObject canvas;

    [SerializeField]
    private float messageFadeInTime = 1.0f;
    [SerializeField]
    private float messageFadeOutTime = 2.0f;
    [SerializeField]
    private float messageLineY = -50;

    [SerializeField]
    private float roomBorderTolerance = 1f;
    [SerializeField]
    private float roomWidth = 10.5f;

    [SerializeField]
    private float roomHeight = 6.0f;

    [SerializeField]
    private PlayerController player;

    private Stack<GameMode> modeStack = new Stack<GameMode>();

    public static GameControl instance;
    private Vector2 cameraTile = new Vector2(-1, 0);
    private Transform camTx;
    private Text keyHintText;
    internal Brain brain; // Should always be internal! :D
    private AudioSource audioSource;
    private List<MessageLine> messageLines = new List<MessageLine>();
    private List<MessageLine> activeMessageLines = new List<MessageLine>();
    private float keyHintVisibility;
    private Vector3 hintPosition;
    private bool keyHintVisible;

    internal Interactable focusedInteractable;
    private float clearTime;
    private bool cameraMoving;
    private float shiftDownTime;

    public static float volSound {
        get {
            return Mathf.Pow(soundVolume / 5f, 2);
        }
    }
    public static float volMusic {
        get {
            return Mathf.Pow(musicVolume / 5f, 2);
        }
    }

    public static bool isWeb {
        get {
            return Application.platform == RuntimePlatform.WebGLPlayer;
        }
    }

    public struct MessageLine {
        public string text;
        public GameObject display;
        public float startTime;
        public Color color;
        public float stayTime;
        internal RectTransform rectTransform;
        internal Text textDisplay;
        internal Color colorInvisible;
        internal SpeechType speechType;
        internal Brain.MindPart mindPart;
    }

    void Awake() {
        // if (instance != null && instance != this) {
        //     Destroy(gameObject);
        //     return;
        // }
        // DontDestroyOnLoad(gameObject);
        instance = this;

        cameraTile = new Vector2(0, 0);
        camTx = Camera.main.transform;

        keyHintText = keyHint.GetComponent<Text>();
        keyHintText.color = new Color(0, 0, 0, 0);
        keyHintVisibility = 0;
    }

    void Start () {
        if (!Application.isEditor) {
            if (!isWeb) {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }

            player.position = new Vector2(-22f, -3.5f);
        }

        modeStack.Push(GetComponent<MainGameMode>());
        modeStack.Peek().EnterMode(true);
        brain = GetComponent<Brain>();
        audioSource = GetComponent<AudioSource>();

        if (Application.isEditor) {
            gameStarted = true;
            musicVolume = 0;
            ApplyMusicVolume();
        }
        if (!gameStarted) {
            ShowMenu();
        }

        ApplyMusicVolume();

        if (Application.isEditor) {
            TestConsistency();
        }
	}

    internal void Quit() {
#if UNITY_EDITOR
        if (Application.isEditor) {
            EditorApplication.isPlaying = false;
        } else
#endif
        {
            Application.Quit();
        }
    }

    private void ShowMenu() {
        MenuGameMode menu = GetComponent<MenuGameMode>();
        PushGameMode(menu);
    }

    internal void ApplyMusicVolume() {
        if (audioSource != null) {
            audioSource.volume = volMusic;
        }
    }

    private void TestConsistency() {
        // Check all texts
        List<TalkInteractable> talks = FindComponents<TalkInteractable>();
        foreach (TalkInteractable ti in talks) {
            if (ti.text != null)
                TalkGameMode.LetsTalkAbout(gameObject, null, ti.text.text, true, false, false);
            else
                Debug.LogError("Missing text in interactive: " + ti.gameObject.name);
        }
    }

    public List<T> FindComponents<T>() {
        List<T> list = new List<T>();
        GameObject[] rootObjects = SceneManager.GetActiveScene().GetRootGameObjects();
        foreach (GameObject go in rootObjects) {
            FindComponents<T>(list, go);
        }
        return list;
    }

    private void FindComponents<T>(List<T> list, GameObject go) {
        T[] objs = go.GetComponents<T>();
        list.AddRange(objs);
        for (int i=0; i<go.transform.childCount; i++) {
            Transform child = go.transform.GetChild(i);
            FindComponents<T>(list, child.gameObject);
        }
    }

    public void ClearStack() {
        Input.ResetInputAxes();
        while (modeStack.Count > 1)
            modeStack.Pop();
    }

    public void PushGameMode(GameMode mode) {
        Input.ResetInputAxes();
        modeStack.Peek().LeaveMode(false);
        modeStack.Push(mode);
        mode.EnterMode(true);
    }

    public void PopGameMode() {
        Input.ResetInputAxes();
        modeStack.Pop().LeaveMode(true);
        modeStack.Peek().EnterMode(false);
    }

    internal void HideHint() {
        keyHintVisible = false;
    }

    internal void ShowHint(string text, Vector3 pos) {
        hintPosition = pos;
        keyHintVisible = true;
        keyHintText.text = text;
        UpdateHintPosition();
    }

    private void UpdateHintPosition() {
        keyHint.GetComponent<RectTransform>().anchoredPosition = ToCanvas(hintPosition);
    }

	void Update () {
        // Recompile check
        if (instance == null) {
            Quit();
        }

        // Screenshot
		if (Input.GetKeyDown(KeyCode.P)) {
            string workingDir = Directory.GetCurrentDirectory();
            DateTime dt = DateTime.Now;
            string filename = workingDir + "/" + "snap_" + string.Format("{0:D4}{1:D2}{2:D2}_{3:D2}{4:D2}{5:D2}", dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second) + ".png";
            ScreenCapture.CaptureScreenshot(filename);
            GameControl.instance.AddMessage("Screenshot saved to:\n" + filename, new Color(1, 0, 1), SpeechType.NONE);
        }

        // Skip active message
        if (KeyDown(SKIP_MESSAGE_KEYS)) {
            shiftDownTime = Time.time;
        } else if (KeyUp(SKIP_MESSAGE_KEYS)) {
            if (Time.time - shiftDownTime < 0.3f)
                SkipCurrentMessage();
        }

        // Escape -> menu
        if (IsGameMode() && KeyDown(GameControl.MENU_KEYS) && !GameControl.instance.GetPlayer().Blocked(GameControl.MENU_KEYS)) {
            GameControl.instance.GetPlayer().Block(GameControl.MENU_KEYS);
            ShowMenu();
        }

        // Do actual game mode stuff
        modeStack.Peek().UpdateMode();

        // Move camera
        Vector2 playerPos = player.position - new Vector2(cameraTile.x * roomWidth, cameraTile.y * roomHeight);
        Vector2 playerVelocity = player.velocity;
        float minVeloc = 0.01f;
        int dx = 0, dy = 0;
        if (playerPos.x >= roomWidth/2 - Tolerance(playerVelocity.x, minVeloc)) {
            dx = 1;
        } else if (playerPos.x <= -roomWidth/2 + Tolerance(-playerVelocity.x, minVeloc)) {
            dx = -1;
        }
        // stop moving vertically on the right side (for sign gag)
        if (cameraTile.x <= 1) {
            if (playerPos.y >= roomHeight/2 - Tolerance(playerVelocity.y, minVeloc)) {
                dy = 1;
            } else if (playerPos.y <= -roomHeight/2 + Tolerance(-playerVelocity.y, minVeloc)) {
                dy = -1;
            }
        }
        if (dx != 0 || dy != 0) {
            cameraTile += new Vector2(dx, dy);
            cameraMoving = true;
        }
        if (cameraMoving) {
            Vector3 cameraDestination = new Vector3(cameraTile.x * roomWidth, cameraTile.y * roomHeight, -10);
            Vector3 cameraCurrent = camTx.localPosition;
            float moveSpeed = roomWidth + (cameraCurrent - cameraDestination).magnitude*2;
            Vector3 pos = camTx.localPosition;
            camTx.localPosition = Vector3.MoveTowards(cameraCurrent, cameraDestination, moveSpeed * Time.unscaledDeltaTime);
            UpdateHintPosition();
            if ((pos - camTx.localPosition).sqrMagnitude == 0)
                cameraMoving = false;
        }

        // Key hint
        bool hasValidHint = keyHintVisible && IsGameMode() && !(BLOCK_ACTION_WHILE_MESSAGE_IS_VISIBLE && HasPendingMessages(false)) && (focusedInteractable == null || !focusedInteractable.CoolingDown());
        keyHintVisibility = Mathf.Min(1, Mathf.Max(0, keyHintVisibility + (hasValidHint ? 2 : -4) * Time.unscaledDeltaTime));
        if (keyHint.activeSelf != keyHintVisibility > 0)
            keyHint.SetActive(keyHintVisibility > 0);
        keyHintText.color = Color.Lerp(GameControl.COLOR_INVISIBLE, Color.black, keyHintVisibility);

        // Update message display
        bool freeSlot = true;
        for (int i=activeMessageLines.Count-1; i>=0; i--) {
            MessageLine dl = activeMessageLines[i];
            float deltaTime = Time.unscaledTime - dl.startTime;
            Vector3 pos = dl.rectTransform.anchoredPosition3D;
            pos.z = 0f;
            if (deltaTime < messageFadeInTime) {
                if (dl.startTime >= clearTime)
                    freeSlot &= false;
                float f = deltaTime / messageFadeInTime;
                pos.y = messageLineY + Mathf.Pow(1-f, 2) * 100;
                dl.textDisplay.color = Color.Lerp(dl.colorInvisible, dl.color, f);
            } else if (deltaTime - messageFadeInTime < dl.stayTime) {
                if (dl.startTime >= clearTime)
                    freeSlot &= false;
                dl.textDisplay.color = dl.color;
                pos.y = messageLineY;
            } else if (deltaTime - messageFadeInTime - dl.stayTime < messageFadeOutTime) {
                float f = (deltaTime - messageFadeInTime - dl.stayTime) / messageFadeOutTime;
                if (dl.startTime >= clearTime)
                    freeSlot &= f > 0.25f;
                dl.textDisplay.color = Color.Lerp(dl.colorInvisible, dl.color, 1-f);
                pos.y = messageLineY - Mathf.Pow(f, 2) * 250;
            } else {
                activeMessageLines.Remove(dl);
                Destroy(dl.display);
            }
            dl.rectTransform.anchoredPosition3D = pos;
        }
        if (freeSlot && messageLines.Count > 0) {
            MessageLine dl = messageLines[0];

            // Only show remember/forget messages if it is still true
            bool skipMessage = false;
            switch (dl.speechType) {
                case SpeechType.SYSTEM_REMEMBER:
                    if (dl.mindPart != null && brain.CanRemember(dl.mindPart))
                        OneShotAudio.PlayClip(GameControl.instance.soundRemember, Vector3.zero, 0f, 0.4f);
                    else
                        skipMessage = true;
                    break;
                case SpeechType.SYSTEM_FORGET:
                    if (dl.mindPart != null && !brain.CanRemember(dl.mindPart))
                        OneShotAudio.PlayClip(GameControl.instance.soundForget, Vector3.zero, 0f, 0.4f);
                    else
                        skipMessage = true;
                    break;
            }

            if (!skipMessage) {
                dl.startTime = Time.unscaledTime;
                dl.display.SetActive(true);
                activeMessageLines.Add(dl);
                messageLines.RemoveAt(0);
            }
        }
        // Show ghosty vision of next message on top
        if (messageLines.Count > 0 != pendingMessageDisplay.activeSelf) {
            pendingMessageDisplay.SetActive(messageLines.Count > 0);
        }
	}

    private float Tolerance(float speed, float minVeloc) {
        return speed > minVeloc ? roomBorderTolerance : -1.75f;
    }

    void FixedUpdate() {
        modeStack.Peek().FixedUpdateMode();
    }

    public Vector2 ToCanvas(Vector3 pos) {
        Vector3 screenPos = Camera.main.WorldToScreenPoint(pos);
        screenPos.x *= 1920f / Screen.width;
        screenPos.y *= 1080f / Screen.height;
        return screenPos;
    }

    internal bool CanJump() {
        return brain.CanRemember(Brain.Memory.JUMP);
    }

    internal static bool IsGameMode() {
        return IsMode<MainGameMode>();
    }

    internal static bool IsMenuMode() {
        return IsMode<MenuGameMode>();
    }

    internal static bool IsTalkMode() {
        return IsMode<TalkGameMode>();
    }

    internal static bool IsMode<T>() {
        return instance.modeStack.Peek() is T;
    }

    internal void ClearPendingMessages() {
        AddMessage(null, Color.white, SpeechType.SYSTEM_SILENT, true);
    }

    internal void AddMessage(string msg, Color color, SpeechType speech, bool clear = false, Brain.MindPart mindPart = null) {
        // Ignore messages that are already queued
        if (msg != null) {
            msg = msg.Replace("\\n", "\n");
        }
        // This has weird side effects depending on the timing, e.g. adding the list of message again after only the first few of the previous one were shown
        // if (activeMessageLines.Exists(delegate(MessageLine line) {
        //         return line.text == msg;
        //     }) || messageLines.Exists(delegate(MessageLine line) {
        //         return line.text == msg;
        //     })) {
        //     return;
        // }

        if (clear) {
            // Keep memory messages
            for (int i=messageLines.Count-1; i>=0; i--) {
                if (messageLines[i].speechType == SpeechType.SYSTEM_REMEMBER || messageLines[i].speechType == SpeechType.SYSTEM_FORGET)
                    continue;
                Destroy(messageLines[i].display);
                messageLines.RemoveAt(i);
            }
            // messageLines.Clear();
            clearTime = Time.unscaledTime;
        }

        if (msg != null) {
            MessageLine messageLine = new MessageLine();
            messageLine.color = color;
            messageLine.mindPart = mindPart;
            messageLine.colorInvisible = new Color(color.r, color.g, color.b, 0);
            messageLine.startTime = 0;
            messageLine.display = Instantiate<GameObject>(messagePrefab);
            messageLine.display.transform.SetParent(messagePrefab.transform.parent, false);
            messageLine.display.transform.localPosition = messagePrefab.transform.localPosition;
            messageLine.display.transform.localScale = Vector3.one * 0.7f;
            messageLine.speechType = speech;
            messageLine.text = speech.ToString().ToUpper().StartsWith("SYSTEM") ? "<i>" + msg + "</i>" : msg;
            messageLine.stayTime = 2 + msg.Length * 0.05f;
            messageLine.rectTransform = messageLine.display.GetComponent<RectTransform>();
            messageLine.textDisplay = messageLine.display.GetComponent<Text>();
            messageLine.textDisplay.text = messageLine.text;
            messageLine.textDisplay.color = new Color(0, 0, 0, 0);
            messageLines.Add(messageLine);
        }

        if (clear) {
            SkipCurrentMessage();
        }
    }

    internal void SkipCurrentMessage() {
        for (int i=0; i<activeMessageLines.Count; i++) {
            MessageLine dl = activeMessageLines[i];
            dl.startTime = Mathf.Min(dl.startTime, Time.unscaledTime - dl.stayTime - messageFadeInTime);
            activeMessageLines[i] = dl;
        }
    }

    internal bool HasPendingMessages(bool includeCurrent = true) {
        return ((includeCurrent ? activeMessageLines.Count : 0) + messageLines.Count) > 0;
    }

    internal PlayerController GetPlayer() {
        return player;
    }

    internal void SetCameraTile(int x, int y) {
        cameraTile = new Vector2(x, y);
    }

    internal Vector2 GetCameraTile() {
        return cameraTile;
    }

    public static bool Key(params KeyCode[] codes) {
        foreach (KeyCode code in codes)
            if (Input.GetKey(code))
                return true;
        return false;
    }

    public static bool KeyDown(params KeyCode[] codes) {
        foreach (KeyCode code in codes)
            if (Input.GetKeyDown(code))
                return true;
        return false;
    }

    public static bool KeyUp(params KeyCode[] codes) {
        foreach (KeyCode code in codes)
            if (Input.GetKeyUp(code))
                return true;
        return false;
    }
}
