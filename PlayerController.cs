﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    [SerializeField]
    private float floorCheckDistance = 0.1f;
    [SerializeField]
    private float floorCheckHeight = 0.1f;
    [SerializeField]
    private float runForce = 20f;
    [SerializeField]
    private float moveForce = 10f;
    [SerializeField]
    private float jumpForce = 20f;
    [SerializeField]
    private float activateKeyBlockTime = 0.25f;

    [SerializeField]
    private float maxSpeed = 8f;
    [SerializeField]
    private float maxSpeedRun = 28f;
    [SerializeField]
    private float slowDown = 20f;

    [SerializeField]
    [Tooltip("Standing, Jump, Land, Run1, Run2, Run3, Run4")]
    private Sprite[] poses;
    [SerializeField]
    private AudioClip[] walkSounds;
    [SerializeField]
    private float stepWidth = 0.2f;

    private SpriteRenderer sprite;
    internal Rigidbody2D body;
    private BoxCollider2D colly;

    private int currentPose = -1;

    private Transform tx;
    private List<KeyCode> bufferedKeys = new List<KeyCode>();

    private Dictionary<KeyCode, float> downKeys = new Dictionary<KeyCode, float>();
    private Dictionary<KeyCode, float> blockedKeys = new Dictionary<KeyCode, float>();

    private float bufferTime = 0.2f;
    private int hSpeed;
    private bool grounded;
    private bool jump;
    private bool jumped;
    private float landedTime;
    private Vector3 visualVelocity;
    private Vector3 lastPosition;
    private Vector2 playerStartPosition;
    private float speedFac;
    private Stairway lastStairWay;
    private bool running;
    private float movingTime;
    private float walkStartPosition;

    public Vector2 position { get {
            return tx.position;
        }
        set {
            tx.position = new Vector3(value.x, value.y, tx.position.z);
        }
    }

    public Vector2 feetPosition { get {
            return new Vector2(tx.position.x, colly.bounds.min.y - colly.edgeRadius);
            //return tx.position + new Vector3(0, -height / 2, 0);
        }
    }

    public Vector2 velocity { get {
            return visualVelocity;//body.velocity;
        }
    }

    void Awake() {
        sprite = GetComponent<SpriteRenderer>();
        body = GetComponent<Rigidbody2D>();
        colly = GetComponent<BoxCollider2D>();
        tx = transform;
        bufferedKeys.AddRange(GameControl.JUMP_KEYS);
        bufferedKeys.AddRange(GameControl.DOWN_KEYS);
        bufferedKeys.AddRange(GameControl.ACTIVATE_KEYS);
    }

    void Start() {
        playerStartPosition = position;
    }

    internal void ResetPosition() {
        StopBody();
        tx.localPosition = playerStartPosition;
        body.isKinematic = false;
    }

    internal void StopBody() {
        body.isKinematic = true;
        body.velocity = Vector2.zero;
        body.angularVelocity = 0;
        body.Sleep();
    }

    public Vector2 GetStartPosition() {
        return playerStartPosition;
    }

    internal void SetFaceDirection(bool right) {
        sprite.flipX = !right;
    }

	// Update is called once per frame
	void Update () {
        // Debug.DrawLine(feetPosition + Vector2.left * 0.5f, feetPosition + Vector2.right * 0.5f, Color.blue, 1, false);
        if (!GameControl.IsGameMode())
            return;

        // Buffered keys
        float t = 0;
        float time = Time.time;
        foreach (KeyCode code in bufferedKeys) {
            if (Input.GetKeyDown(code)) {
                downKeys[code] = Time.time;
            } else if (downKeys.TryGetValue(code, out t) && time - t > bufferTime) {
                downKeys.Remove(code);
            }
        }

        // Controls
        hSpeed = 0;
        if (GameControl.Key(GameControl.LEFT_KEYS)) {
            hSpeed = -1;
            SetFaceDirection(false);
        } else if (GameControl.Key(GameControl.RIGHT_KEYS)) {
            hSpeed = 1;
            SetFaceDirection(true);
        }
        if (KeyDown(GameControl.JUMP_KEYS) && GameControl.instance.CanJump()) {
            jump = true;
        }
        if (KeyDown(GameControl.DOWN_KEYS) && lastStairWay != null) {
            lastStairWay.Passthrough();
            body.AddForce(Vector2.down * 50);
            Consume(GameControl.DOWN_KEYS);
        }
        running = GameControl.Key(GameControl.RUN_KEYS) && GameControl.instance.brain.CanRemember(Brain.Memory.RUN);

        // Action
        if (GameControl.KeyDown(GameControl.ACTIVATE_KEYS) && !Blocked(GameControl.ACTIVATE_KEYS)) {
            Consume(GameControl.ACTIVATE_KEYS);
            Block(GameControl.ACTIVATE_KEYS);
            Interactable i = GameControl.instance.focusedInteractable;
            if (i != null && !i.CoolingDown()) {
                if (!GameControl.BLOCK_ACTION_WHILE_MESSAGE_IS_VISIBLE || !GameControl.instance.HasPendingMessages(false)) {
                    i.Trigger();
                }
            } else {
                GameControl.instance.SkipCurrentMessage();
            }
        }
        if (GameControl.KeyUp(GameControl.ACTIVATE_KEYS)) {
            Unblock(GameControl.ACTIVATE_KEYS);
        }
	}

    void FixedUpdate() {
        float time = Time.time;
        visualVelocity = tx.position - lastPosition;
        lastPosition = tx.position;

        // set pose
        Vector2 feetPosition = this.feetPosition;
        float speed = Mathf.Abs(visualVelocity.x);
        if (speed > 0.01f) {
            if (currentPose < 3)
                walkStartPosition = feetPosition.x;
            movingTime = time;
        }
        int pose = -1;
        if (jumped) {
            pose = 1;
        } else if (time - landedTime < 0.3f) {
            pose = 2;
        } else if (time - movingTime < 0.075f) {
            pose = 3 + (1 + Mathf.FloorToInt((100 + (sprite.flipX ? 1 : -1) * (feetPosition.x - walkStartPosition)) / stepWidth)) % 4;
            if (pose != currentPose && pose % 2 == 0) {
                // Walk sound
                OneShotAudio.PlayClip(walkSounds[UnityEngine.Random.Range(0, walkSounds.Length)], Vector3.zero, 0, 0.8f + Mathf.Min(1, speed / maxSpeedRun) * 0.2f, 0.8f + Mathf.Min(1, speed / maxSpeedRun));
            }
        } else {
            pose = 0;
        }
        if (pose != currentPose) {
            currentPose = pose;
            sprite.sprite = poses[pose];
        }

        if (!GameControl.IsGameMode())
            return;

        // Ground check
        Vector2 veloc = body.velocity;
        Bounds b = colly.bounds;
        grounded = false;
        if (grounded || veloc.y <= 0) {
            // Debug.DrawLine(feetPosition + new Vector2(-b.size.x/2, floorCheckHeight - floorCheckDistance), feetPosition + new Vector2(b.size.x/2, floorCheckHeight-floorCheckDistance), Color.green, 1);
            // Debug.DrawLine(feetPosition + new Vector2(-b.size.x/2, -floorCheckDistance), feetPosition + new Vector2(b.size.x/2, -floorCheckDistance), Color.green, 1);
            RaycastHit2D hit = Physics2D.BoxCast(feetPosition + new Vector2(0, floorCheckHeight/2), new Vector2(b.size.x, floorCheckHeight), 0, Vector2.down, floorCheckDistance, GameControl.LAYERMASK_FLOOR, -1f, 1f);
            grounded = (hit.collider != null);
            lastStairWay = hit.collider != null ? hit.collider.gameObject.GetComponent<Stairway>() : null;
            // Debug.Log("Floor (" + grounded + "): " + (hit.collider != null ? hit.collider.gameObject.name : "-"));
        }

        // Slope check
        if (grounded) {
            if (jumped) {
                OneShotAudio.PlayClip(walkSounds[5], Vector3.zero, 0, 1f, 1f);
                jumped = false;
                landedTime = time;
            }
            float yFac = 1;
            RaycastHit2D hit = Physics2D.Raycast(feetPosition + new Vector2(b.size.x, 0), Vector2.down, floorCheckDistance, GameControl.LAYERMASK_FLOOR);
            lastStairWay = hit.collider != null ? hit.collider.gameObject.GetComponent<Stairway>() : null;
            yFac = Math.Min(yFac, hit.normal.y);
            hit = Physics2D.Raycast(feetPosition - new Vector2(b.size.x, 0), Vector2.down, 0.1f, GameControl.LAYERMASK_FLOOR);
            if (lastStairWay == null)
                lastStairWay = hit.collider != null ? hit.collider.gameObject.GetComponent<Stairway>() : null;
            yFac = Math.Min(yFac, hit.normal.y);
            speedFac = 1 + (1-Mathf.Pow(yFac, 2)) * 2;
        }
        if (!grounded) {
            WakeUp();
        }

        // Horizontal movement
        body.AddForce(Vector2.right * hSpeed * (running ? runForce : moveForce) * speedFac, ForceMode2D.Force);

        // Restrict speed and slow down
        veloc.x = Mathf.Min(Mathf.Max(0, Mathf.Abs(veloc.x) - (hSpeed == 0 ? Time.deltaTime * slowDown : 0)), running ? maxSpeedRun : maxSpeed) * Mathf.Sign(veloc.x);

        // Stop on slope
        // body.drag = (grounded && hSpeed == 0) ? 50 : 0;
        if (hSpeed == 0 && grounded && speedFac > 1) {
            body.Sleep();
            veloc.x = 0;
            veloc.y = 0;
        }

        // Apply new velocity
        body.velocity = veloc;

        // Jump
        if (jump) {
            jump = false;
            jumped = true;
            Consume(GameControl.JUMP_KEYS);

            // Jump if grounded
            if (grounded) {
                grounded = false;
                jump = false;
                body.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            }
        }

        // Ded?
        if (position.y < -12) {
            GameMode mode = new YouDedGameMode("You forgot how to... everything.", "This will teach you not to run into unknown rooms!", "Of course you will have forgotten this in a moment...");
            GameControl.instance.PushGameMode(mode);
        }


    }

    public void WakeUp() {
        body.isKinematic = false;
        body.WakeUp();
    }

    public bool KeyDown(params KeyCode[] codes) {
        foreach (KeyCode code in codes)
            if (downKeys.ContainsKey(code))
                return true;
        return GameControl.KeyDown(codes);
    }

    internal void Consume(params KeyCode[] keys) {
        foreach (KeyCode code in keys)
            if (downKeys.ContainsKey(code))
                downKeys.Remove(code);
    }

    internal void Block(params KeyCode[] keys) {
        foreach (KeyCode code in keys)
            blockedKeys[code] = Time.time;
    }

    internal void Unblock(params KeyCode[] keys) {
        foreach (KeyCode code in keys)
            blockedKeys.Remove(code);
    }

    internal bool Blocked(params KeyCode[] keys) {
        foreach (KeyCode code in keys)
            if (blockedKeys.ContainsKey(code) && (Time.time - blockedKeys[code]) < activateKeyBlockTime)
                return true;
        return false;
    }

}
