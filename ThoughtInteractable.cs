﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThoughtInteractable : MonoBehaviour {

    [SerializeField]
    internal TalkText text;

    void OnTriggerEnter2D(Collider2D other) {
        string[] msgs = text.text.Replace("*", "").Split(';');
        foreach (string m in msgs)
            GameControl.instance.AddMessage(m, GameControl.MESSAGECOLOR_NPC, GameControl.SpeechType.SYSTEM_SILENT, m == msgs[0]);
        gameObject.SetActive(false);
    }

}
