using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class SpecialTransition : TransitionGameMode {
    private SpecialEvent specialEvent;

    void Awake() {
        specialEvent = GetComponent<SpecialEvent>();
    }

    internal override void FinishTransition() {
    }

    internal override void PlayTransition(float f) {
    }

    internal override void PrepareTransition() {
        duration = 0.001f;
        if (transitionForward && specialEvent != null) {
            specialEvent.Trigger();
        } else if (!transitionForward && specialEvent != null) {
            specialEvent.DeTrigger();
        }
    }
}