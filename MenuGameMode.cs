using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuGameMode : MonoBehaviour, GameMode {

    [SerializeField]
    private RawImage bgImage;
    [SerializeField]
    private RawImage titleImage;
    [SerializeField]
    private Vector2 menuBrainPosition = new Vector2(-70, 30);
    [SerializeField]
    private Vector3 menuBrainScale = new Vector3(8, 8, 1);

    [SerializeField]
    private Texture textureMenuPoint;
    [SerializeField]
    private Texture textureMenuPointSelected;
    [SerializeField]
    private Texture[] menuBarStateTextures;

    [SerializeField]
    private RawImage musicVolumeBar;
    [SerializeField]
    private RawImage soundVolumeBar;

    [SerializeField]
    private RawImage[] menuPoints;

    private Color bgColor;
    private Color bgColorInvisible;
    [SerializeField]
    private string[] menuPointsActions;

    private int currentOption;

    private float menuVisibility = 0;
    private Vector2 brainPosition;
    private RectTransform brainRoot;

    void Awake() {
        bgColor = bgImage.color;
        bgColorInvisible = new Color(bgColor.r, bgColor.g, bgColor.b, 0);
        bgImage.gameObject.SetActive(false);
    }

    public void EnterMode(bool push) {
        if (GameControl.gameStarted)
            currentOption = 1; // continue
        musicVolumeBar.texture = menuBarStateTextures[GameControl.musicVolume];
        soundVolumeBar.texture = menuBarStateTextures[GameControl.soundVolume];
        brainPosition = GameControl.instance.brain.position;
        brainRoot = GameControl.instance.brain.brainRoot;
        ApplyOptionSelection();
        LateUpdate(); // to set color
    }

    private void ApplyOptionSelection() {
        for (int i=0; i<menuPoints.Length; i++) {
            menuPoints[i].texture = (i == currentOption) ? textureMenuPointSelected : textureMenuPoint;
        }
    }

    public void FixedUpdateMode() {
    }

    public void LeaveMode(bool pop) {
        Input.ResetInputAxes();
    }

    public void UpdateMode() {
        string command = menuPointsActions[currentOption];
        if (GameControl.KeyDown(GameControl.UP_KEYS)) {
            currentOption = (currentOption + menuPoints.Length-1) % menuPoints.Length;
            ApplyOptionSelection();
            OneShotAudio.PlayClip(GameControl.instance.soundMenuSelect, Vector3.zero, 0f, 1f);
        } else if (GameControl.KeyDown(GameControl.MENU_KEYS) && !GameControl.instance.GetPlayer().Blocked(GameControl.MENU_KEYS)) {
            GameControl.instance.GetPlayer().Block(GameControl.MENU_KEYS);
            GameControl.instance.PopGameMode();
            OneShotAudio.PlayClip(GameControl.instance.soundMenuSelect, Vector3.zero, 0f, 1f);
        } else if (GameControl.KeyDown(GameControl.LEFT_KEYS) || GameControl.KeyDown(GameControl.RIGHT_KEYS)) {
            int dir = GameControl.KeyDown(GameControl.LEFT_KEYS) ? -1 : 1;
            if (command == "music") {
                //AudioSource.PlayClipAtPoint(GameControl.instance.soundMenuActivate, GameControl.instance.GetPlayer().position, GameControl.volSound);
                AudioSource.PlayClipAtPoint(GameControl.instance.soundMenuSelect, GameControl.instance.GetPlayer().position, GameControl.volSound);
                GameControl.musicVolume = Mathf.Max(0, Mathf.Min(5, GameControl.musicVolume + dir));
                GameControl.instance.ApplyMusicVolume();
                musicVolumeBar.texture = menuBarStateTextures[GameControl.musicVolume];
            } else if (command == "sound") {
                //AudioSource.PlayClipAtPoint(GameControl.instance.soundMenuActivate, GameControl.instance.GetPlayer().position, GameControl.volSound);
                GameControl.soundVolume = Mathf.Max(0, Mathf.Min(5, GameControl.soundVolume + dir));
                // AudioSource.PlayClipAtPoint(GameControl.instance.soundRemember, GameControl.instance.GetPlayer().position, GameControl.volSound);
                OneShotAudio.PlayClip(GameControl.instance.soundRemember, Vector3.zero, 0f, 0.4f);
                soundVolumeBar.texture = menuBarStateTextures[GameControl.soundVolume];
            }
        } else if (GameControl.KeyDown(GameControl.DOWN_KEYS)) {
            currentOption = (currentOption + 1) % menuPoints.Length;
            ApplyOptionSelection();
            AudioSource.PlayClipAtPoint(GameControl.instance.soundMenuSelect, GameControl.instance.GetPlayer().position, GameControl.volSound);
        } else if (GameControl.KeyDown(GameControl.ACTIVATE_KEYS)) {
            if (command.StartsWith("http")) {
                OneShotAudio.PlayClip(GameControl.instance.soundMenuActivate, Vector3.zero, 0f, 1f);
                if (GameControl.isWeb) {
                    Application.ExternalEval("window.open(\""+command+"\")");
                } else {
                    Application.OpenURL(command);
                }
            } else if (command == "exit") {
                GameControl.instance.Quit();
            } else if (command == "continue") {
                OneShotAudio.PlayClip(GameControl.instance.soundMenuActivate, Vector3.zero, 0f, 1f);
                GameControl.gameStarted = true;
                GameControl.instance.PopGameMode();
            } else if (command == "newgame") {
                OneShotAudio.PlayClip(GameControl.instance.soundMenuActivate, Vector3.zero, 0f, 1f);
                bool wasStarted = GameControl.gameStarted;
                GameControl.gameStarted = true;
                if (!wasStarted)
                    GameControl.instance.PopGameMode();
                else
                    SceneManager.LoadScene("game");
            } else if (command == "music") {
                //AudioSource.PlayClipAtPoint(GameControl.instance.soundMenuActivate, GameControl.instance.GetPlayer().position, GameControl.volSound);
                AudioSource.PlayClipAtPoint(GameControl.instance.soundMenuSelect, GameControl.instance.GetPlayer().position, GameControl.volSound);
                GameControl.musicVolume = (GameControl.musicVolume + 1) % 6;
                GameControl.instance.ApplyMusicVolume();
                musicVolumeBar.texture = menuBarStateTextures[GameControl.musicVolume];
            } else if (command == "sound") {
                //AudioSource.PlayClipAtPoint(GameControl.instance.soundMenuActivate, GameControl.instance.GetPlayer().position, GameControl.volSound);
                GameControl.soundVolume = (GameControl.soundVolume + 1) % 6;
                OneShotAudio.PlayClip(GameControl.instance.soundRemember, Vector3.zero, 0f, 0.4f);
                // AudioSource.PlayClipAtPoint(GameControl.instance.soundRemember, GameControl.instance.GetPlayer().position, GameControl.volSound);
                soundVolumeBar.texture = menuBarStateTextures[GameControl.soundVolume];
            }
        }
    }

    public void LateUpdate() {
        bool inMenuMode = GameControl.IsMenuMode();
        if ((inMenuMode && menuVisibility < 1) || (!inMenuMode && menuVisibility > 0)) {
            menuVisibility = Mathf.Max(0, Mathf.Min(1, menuVisibility + (inMenuMode ? .75f : -1f) * Time.unscaledDeltaTime));
            if (menuVisibility == 0 != !bgImage.gameObject.activeSelf)
                bgImage.gameObject.SetActive(menuVisibility != 0);
            bgImage.color = Color.Lerp(bgColorInvisible, bgColor, Mathf.Pow(menuVisibility, 2));
            Color whiteFade = Color.Lerp(GameControl.COLOR_INVISIBLE_WHITE, Color.white, Mathf.Pow(menuVisibility, 0.5f));
            titleImage.color = whiteFade;
            foreach (RawImage point in menuPoints)
                point.color = whiteFade;
            soundVolumeBar.color = whiteFade;
            musicVolumeBar.color = whiteFade;

            brainRoot.anchoredPosition = Vector2.Lerp(brainPosition, menuBrainPosition, Mathf.SmoothStep(0, 1, menuVisibility));
            brainRoot.localScale = Vector3.Lerp(new Vector3(3, 3, 1), menuBrainScale, Mathf.SmoothStep(0, 1, menuVisibility));
        }
    }
}