﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * No time to build a scriptable object, using this instead
 */
public class TalkText : MonoBehaviour {
    [SerializeField]
    [Multiline(20)]
    internal string text;
}
