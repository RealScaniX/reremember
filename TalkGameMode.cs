using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class TalkGameMode : GameMode {

    public class DialogOption {
        public string nested;
        public Brain.MindPart condition;
        public string text;
        public string[][] response; //[options][lines]
    }

    private List<DialogOption> options = new List<DialogOption>();
    private bool left;
    private int currentOption;

    private List<GameObject> optionPointers = new List<GameObject>();
    private string[][] startupMessage;
    private bool selfTalking;
    private readonly GameObject owner;
    private SpecialEvent special;
    private GameObject dlgRoot;

    /*
        *welcomemsg;welcomemsg2|welcomemsg3
        optionText;condition:response;response2|response3
           nested talk

        texts endings
        ?: question
        ???: irritated
        !: motivating

        UPPERCASE: remember memory with this name

        "back": back out of option

        "special": calls attached special interactable

        hint or memeory: gain this

        \n to linefeed



     */

    public static TalkGameMode LetsTalkAbout(GameObject owner, SpecialEvent special, string text, bool left, bool nested, bool selfTalking) {
        StringReader sr = new StringReader(text);
        string line;
        DialogOption opt = null;
        TalkGameMode mode = new TalkGameMode(owner);
        mode.special = special;
        mode.left = left;
        bool firstLine = true;
        while ((line = sr.ReadLine()) != null) {
            if (string.IsNullOrEmpty(line.Trim()))
                continue;

            if (line.StartsWith("   ")) {
                if (firstLine) {
                    Debug.LogError("First line must not be indented!");
                }
                // Add to nested
                if (opt != null)
                    opt.nested += line.Substring(3) + "\n";
            } else {
                opt = new DialogOption();
                string[] parts = line.Split(new char[]{':'}, 2);
                if (line.StartsWith("*")) {
                    parts = new string[]{null, line.Substring(1)};
                } else {
                    firstLine = false;
                    string option = parts[0];
                    string[] optionParts = option.Split(';');
                    opt.condition = optionParts.Length > 1 ? GameControl.instance.brain.GetMindPart(optionParts[1], true) : null;
                    opt.text = optionParts[0];
                }
                string response = parts[1];
                string[] responseParts = response.Split('|');
                string[][] responses = new string[responseParts.Length][];
                int i = 0;
                foreach (string resp in responseParts) {
                    responses[i++] = resp.Split(';');
                }
                if (line.StartsWith("*")) {
                    mode.startupMessage = responses;
                } else {
                    opt.response = responses;
                    mode.options.Add(opt);
                }
            }
        }

        DialogOption backOption = new DialogOption();
        backOption.text = nested ? "<i><--back...</i>" : "<i>leave</i>";
        mode.options.Add(backOption);

        mode.selfTalking = selfTalking;

        return mode;
    }

    private TalkGameMode(GameObject owner) {
        this.owner = owner;
    }

    public void EnterMode(bool push) {
        Input.ResetInputAxes();

        // Build dialog options
        optionPointers.Clear();
        GameObject dlgOptPrefab = GameControl.instance.dialogOptionPrefab;
        dlgRoot = new GameObject("DialogRoot");
        dlgRoot.transform.parent = dlgOptPrefab.transform.parent;
        dlgRoot.transform.position = Vector3.zero;
        dlgRoot.transform.rotation = Quaternion.identity;
        dlgRoot.transform.localScale = Vector3.one;
        dlgRoot.SetActive(false);
        Transform parent = dlgRoot.transform;
        // 60 (1140 right), 40, y += 60
        int n = options.Count;
        float x = left ? 60 : 1140;
        float y = 40 + 60 * (n-1);
        foreach (DialogOption option in options) {
            GameObject dlgOpt = GameObject.Instantiate<GameObject>(dlgOptPrefab, parent);
            dlgOpt.SetActive(true);
            dlgOpt.GetComponent<RectTransform>().anchoredPosition = new Vector2(x, y);
            Text text = dlgOpt.GetComponentInChildren<Text>();
            if (option.condition == null || GameControl.instance.brain.CanRemember(option.condition)) {
                text.text = option.text;
            } else {
                if (option.condition != null && option.condition.level == 10) {
                    text.color = Color.Lerp(Color.white, GameControl.COLOR_INVISIBLE_WHITE, 0.6f);
                    text.text = "<i>" + option.text + "</i>";
                } else {
                    text.color = Color.white;
                    text.text = "";
                }
            }
            if (option.condition != null) {
                Color color = Brain.colorMap[option.condition.memory];
                Color colorFaded = Color.Lerp(Color.Lerp(color, Color.white, 0.4f), Color.black, 0.5f);
                colorFaded.a = (1 + dlgOpt.GetComponent<RawImage>().color.a) / 2;
                dlgOpt.GetComponent<RawImage>().color = colorFaded;
            }
            optionPointers.Add(dlgOpt.transform.GetChild(1).gameObject);
            y -= 60;
        }

        // Mark first option / Mark last if coming back
        if (push)
            currentOption = 0;
        ApplyPointer();

        // Say hello
        if (push && startupMessage != null) {
            Say(startupMessage);
        }
    }

    private void ApplyPointer() {
        for (int i=0; i<optionPointers.Count; i++)
            optionPointers[i].SetActive(i == currentOption);
    }

    public void FixedUpdateMode() {

    }

    public void LeaveMode(bool pop) {
        // Delete all dialog lines
        GameObject.Destroy(dlgRoot);
        // foreach (GameObject pointer in optionPointers)
        //     GameObject.Destroy(pointer.transform.parent.gameObject);
    }

    public void UpdateMode() {
        bool messagesVisible = GameControl.instance.HasPendingMessages(false);
        if (messagesVisible) {
            if (dlgRoot.activeSelf)
                dlgRoot.SetActive(false);
            if (GameControl.KeyDown(GameControl.ACTIVATE_KEYS) && !GameControl.instance.GetPlayer().Blocked(GameControl.ACTIVATE_KEYS)) {
                GameControl.instance.GetPlayer().Block(GameControl.ACTIVATE_KEYS);
                GameControl.instance.SkipCurrentMessage();
            }
        } else {
            if (!dlgRoot.activeSelf)
                dlgRoot.SetActive(true);
            if (GameControl.KeyDown(GameControl.UP_KEYS)) {
                OneShotAudio.PlayClip(GameControl.instance.soundMenuSelect, Vector3.zero, 0f, 1f);
                currentOption = (currentOption + options.Count-1) % options.Count;
                ApplyPointer();
            } else if (GameControl.KeyDown(GameControl.DOWN_KEYS)) {
                OneShotAudio.PlayClip(GameControl.instance.soundMenuSelect, Vector3.zero, 0f, 1f);
                currentOption = (currentOption + 1) % options.Count;
                ApplyPointer();
            } else if (GameControl.KeyDown(KeyCode.Escape)) {
                OneShotAudio.PlayClip(GameControl.instance.soundMenuActivate, Vector3.zero, 0f, 1f);
                GameControl.instance.ClearPendingMessages();
                currentOption = options.Count-1;
                ApplyPointer();
                GameControl.instance.PopGameMode();
            } else if (GameControl.KeyDown(GameControl.ACTIVATE_KEYS) && !GameControl.instance.GetPlayer().Blocked(GameControl.ACTIVATE_KEYS)) {
                GameControl.instance.GetPlayer().Block(GameControl.ACTIVATE_KEYS);
                OneShotAudio.PlayClip(GameControl.instance.soundMenuActivate, Vector3.zero, 0f, 1f);
                DialogOption option = options[currentOption];
                if (option.condition != null && !GameControl.instance.brain.CanRemember(option.condition)) {
                    GameControl.instance.AddMessage(option.condition.level == 10 ? "<i>I do not know this.</i>" : "<i>I cannot remember this.</i>", GameControl.MESSAGECOLOR_PLAYER, GameControl.SpeechType.IRRITATED, true);
                } else {
                    // Always go back in last option
                    if (currentOption == options.Count - 1) {
                        GameControl.instance.ClearPendingMessages();
                        GameControl.instance.PopGameMode();
                        return;
                    }

                    // Add responses
                    if (option.response != null) {
                        Say(option.response);
                    }

                    // Open nested
                    if (!string.IsNullOrEmpty(option.nested)) {
                        GameMode mode = TalkGameMode.LetsTalkAbout(owner, special, option.nested, left, true, selfTalking);
                        GameControl.instance.PushGameMode(mode);
                    }
                }
            }
        }
    }

    private void Say(string[][] messages) {
        int idx = UnityEngine.Random.Range(0, messages.Length);
        bool first = true;
        foreach (string msg in messages[idx]) {
            string text = msg;

            if (text == "back") {
                GameControl.instance.PopGameMode();
                continue;
            } else if (text == "exit") {
                while (GameControl.IsTalkMode())
                    GameControl.instance.PopGameMode();
                continue;
            } else if (text == "special") {
                if (special != null)
                    special.Trigger();
                continue;
            } else if (text.StartsWith("elevate.")) {
                while (GameControl.IsTalkMode())
                    GameControl.instance.PopGameMode();
                ElevatorTransition elevator = owner.GetComponent<ElevatorTransition>();
                elevator.MoveToLevel(int.Parse(text.Substring("elevate.".Length)));
                continue;
            }

            if (text.ToUpper() == msg && !text.EndsWith("!")) {
                try {
                    // Brain.Memory memory = (Brain.Memory)Enum.Parse(typeof(Brain.Memory), msg, true);
                    // GameControl.instance.brain.Remember(memory);
                    GameControl.instance.brain.Remember(text);
                    continue;
                } catch (Exception) {
                    // Expected
                }
            }

            GameControl.SpeechType type = GameControl.SpeechType.NORMAL;
            if (msg.EndsWith("???")) {
                text = msg.Substring(0, msg.Length - 2);
                type = GameControl.SpeechType.IRRITATED;
            } else if (msg.EndsWith("!")) {
                type = GameControl.SpeechType.MOTIVATING;
            } else if (msg.EndsWith("?")) {
                type = GameControl.SpeechType.QUESTION;
            }
            GameControl.instance.AddMessage(text, selfTalking ? GameControl.MESSAGECOLOR_PLAYER : GameControl.MESSAGECOLOR_NPC, type, first);
            first = false;
        }
    }
}
