﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockedDoorSpecialEvent : SpecialEvent {

    [SerializeField]
    internal GameObject door;

    private float startTime = -1;
    private Vector3 doorStartPosition;

    void Start() {
        doorStartPosition = door.transform.position;
    }

    public override void Trigger() {
        // Kill door
        startTime = Time.time;
    }

    public override void DeTrigger() {
    }

    void Update() {
        // probably slide it up at least
        if (startTime > 0) {
            float f = Mathf.Min(1, (Time.time - startTime) / 1.5f);
            door.transform.position = doorStartPosition + Vector3.up * Mathf.SmoothStep(0, 5f, f);
            if (f == 1) {
                startTime = -1;
                door.SetActive(false);
            }
        }
    }
}
