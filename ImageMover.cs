﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageMover : MonoBehaviour {

    [SerializeField]
    private Vector2 movement = new Vector2(1.6f, 2.5f);

    private RawImage rawImage;
    private float startTime;

    // Use this for initialization
    void Start () {
		rawImage = GetComponent<RawImage>();
        startTime = Time.unscaledTime;
	}

	// Update is called once per frame
	void Update () {
		float x = Snap(Mathf.Repeat(movement.x * (Time.unscaledTime - startTime), 1));
		float y = Snap(Mathf.Repeat(movement.y * (Time.unscaledTime - startTime), 1));
        Rect rect = rawImage.uvRect;
        rect.x = x;
        rect.y = y;
        rawImage.uvRect = rect;
	}

    private float Snap(float v) {
        return v;//(int)(v * 64) / 64f;
    }
}
