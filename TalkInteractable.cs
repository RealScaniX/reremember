﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkInteractable : Interactable {

    [SerializeField]
    internal bool left;

    [SerializeField]
    [Tooltip("True to make the player talk, used for elevator etc.")]
    internal bool selfTalking;

    [SerializeField]
    internal float talkPosition = -1000;

    [SerializeField]
    [Tooltip("Called when \"special\" is used as response message.")]
    internal SpecialEvent special;

    [SerializeField]
    internal TalkText text;

    public override void Trigger() {
        actionProcessor = TalkGameMode.LetsTalkAbout(gameObject, special, text.text, left, false, selfTalking);

        DoorTransition door = GetComponent<DoorTransition>();
        if (door != null) {
            door.destination = actionProcessor;
            actionProcessor = door;
        }

        SoundTransition sound  = GetComponent<SoundTransition>();
        if (sound != null) {
            sound.destination = actionProcessor;
            actionProcessor = sound;
        }

        if (talkPosition != -1000) {
            MovePlayerTransition move = GetComponent<MovePlayerTransition>();
            if (move != null) {
                move.position = gameObject.transform.position.x + talkPosition;
                move.destination = actionProcessor;
                actionProcessor = move;
            }
        }

        SpecialTransition spec = GetComponent<SpecialTransition>();
        if (spec != null) {
            spec.destination = actionProcessor;
            actionProcessor = spec;
        }

        base.Trigger();
    }
}
