﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerDoorSpecialEvent : SpecialEvent {

    [SerializeField]
    internal TalkInteractable otherDoor;

    [SerializeField]
    private TalkInteractable sign;

    [SerializeField]
    private float runDuration = 4f;

    private static float runStartTime = -1;
    private DoorTransition doorScript;

    public override void Trigger() {
        doorScript = otherDoor.GetComponent<DoorTransition>();
        if (runDuration <= 0) {
            runStartTime = -1;
            EnableTalk(true);
        } else {
            OneShotAudio.PlayClip(GameControl.instance.soundPing, Vector3.zero, 0, 1, .7f, 0.5f);
            GameControl.instance.AddMessage("Yes, please?", GameControl.MESSAGECOLOR_NPC, GameControl.SpeechType.SYSTEM_SILENT, true);
            runStartTime = Time.time;
            EnableTalk(true);
        }
    }

    public override void DeTrigger() {
        if (runDuration <= 0) {
            EnableTalk(false);
        }
    }

    private void EnableTalk(bool enable) {
        otherDoor.interactive = enable;
        sign.interactive = !enable;
    }

    void Update() {
        if (runDuration <= 0) {
            return;
        }
        if (runStartTime > -1) {
            float t = Time.time - runStartTime;
            float door = Mathf.Max(0, Mathf.Min(1, Mathf.Min(t, runDuration - t)));
            doorScript.Apply(door);

            if (otherDoor.interactive && t > runDuration - 1) {
                EnableTalk(false);
                GameControl.instance.AddMessage("Mmh... no one there.", GameControl.MESSAGECOLOR_NPC, GameControl.SpeechType.SYSTEM_SILENT, true);
            }

            // Player left the room?
            if (GameControl.instance.GetCameraTile().x > -1) {
                runStartTime = -1;
                EnableTalk(false);
                return;
            }

            if (Time.time - runStartTime > runDuration) {
                runStartTime = -1;
            }
        }
    }
}
