﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Brain : MonoBehaviour {

    public class MindPart {
        public readonly string name;
        public readonly Memory memory;
        public readonly bool unforgettable;
        public readonly int level;
        public RawImage image;

        public MindPart(string name, Memory memory, int level, bool unforgettable = false) {
            this.name = name;
            this.memory = memory;
            this.level = level;
            this.unforgettable = unforgettable;
        }

        public override bool Equals(object obj) {
            if (obj == null || GetType() != obj.GetType()) {
                return false;
            }
            return name == ((MindPart)obj).name;
        }

        public override int GetHashCode() {
            return name.GetHashCode();
        }
    }

    internal MindPart GetMindPart(string name, bool includeAll) {
        MindPart[] parts = includeAll
                ? new MindPart[]{MINDPART_DAUGHTER, MINDPART_JUMP, MINDPART_NAME, MINDPART_RUN, MINDPART_ELEVATOR, MINDPART_DOOR_COMBINATION,
                                 MINDPART_JUMP_CONNECTOR, MINDPART_RUN_CONNECTOR, MINDPART_NAME_CONNECTOR, MINDPART_DOOR_CONNECTOR,
                                 MINDPART_HINT_CLOSEDSTAIRWAY, MINDPART_HINT_DOOR, MINDPART_HINT_ELEVATORCODE, MINDPART_HINT_FLOOR4, MINDPART_HINT_HERNAME}
                : new MindPart[]{MINDPART_DAUGHTER, MINDPART_JUMP, MINDPART_NAME, MINDPART_RUN, MINDPART_ELEVATOR, MINDPART_DOOR_COMBINATION};
        string lname = name.ToLower();
        foreach (MindPart part in parts) {
            if (part.name.ToLower() == lname) {
                return part;
            }
        }
        return null;
    }

    public enum Memory {
        BASE,
        CONNECTOR,
        HINT,
        NAME,
        JUMP,
        RUN,
        ELEVATOR,
        DOOR
    }

    public static readonly MindPart MINDPART_DAUGHTER = new MindPart("daughter", Memory.BASE, 0, true);
    public static readonly MindPart MINDPART_JUMP = new MindPart("jump", Memory.JUMP, 2, false);
    public static readonly MindPart MINDPART_RUN = new MindPart("run", Memory.RUN, 2, false);
    public static readonly MindPart MINDPART_NAME = new MindPart("name", Memory.NAME, 2, false);
    public static readonly MindPart MINDPART_DOOR_COMBINATION = new MindPart("door", Memory.DOOR, 1, false);
    public static readonly MindPart MINDPART_ELEVATOR = new MindPart("elevator", Memory.ELEVATOR, 2, false);
    public static readonly MindPart MINDPART_JUMP_CONNECTOR = new MindPart("jump_connector", Memory.CONNECTOR, 1, false);
    public static readonly MindPart MINDPART_RUN_CONNECTOR = new MindPart("run_connector", Memory.CONNECTOR, 1, false);
    public static readonly MindPart MINDPART_NAME_CONNECTOR = new MindPart("name_connector", Memory.CONNECTOR, 1, false);
    public static readonly MindPart MINDPART_DOOR_CONNECTOR = new MindPart("door_connector", Memory.CONNECTOR, 1, false);

    public static readonly MindPart MINDPART_HINT_DOOR = new MindPart("hint_door", Memory.HINT, 10, true);
    public static readonly MindPart MINDPART_HINT_CLOSEDSTAIRWAY = new MindPart("hint_closedstairway", Memory.HINT, 10, true);
    public static readonly MindPart MINDPART_HINT_ELEVATORCODE = new MindPart("hint_elevatorcode", Memory.HINT, 10, true);
    public static readonly MindPart MINDPART_HINT_FLOOR4 = new MindPart("hint_floor4", Memory.HINT, 10, true);
    public static readonly MindPart MINDPART_HINT_HERNAME = new MindPart("hint_hername", Memory.HINT, 10, true);


    public static readonly Dictionary<Memory, Color> colorMap = new Dictionary<Memory, Color>() {
        {Memory.BASE, Color.gray},
        {Memory.CONNECTOR, Color.white},
        {Memory.HINT, Color.white},
        {Memory.NAME, new Color(1, .4f, 1)},
        {Memory.JUMP, Color.green},
        {Memory.RUN, Color.red},
        {Memory.ELEVATOR, Color.yellow},
        {Memory.DOOR, Color.blue},
    };

    private static readonly Dictionary<Memory, string[]> textsMap = new Dictionary<Memory, string[]>() {
        {Memory.BASE, new string[]{"", ""}},
        {Memory.CONNECTOR, new string[]{"You remembered something.", "You've forgotten something."}},
        {Memory.HINT, new string[]{"You gained new knowledge.", "You became dumber."}},
        {Memory.NAME, new string[]{"Your name is Frederic Welldington\n(or something like that).", "Congratulations!\nYou've forgotten your own name again."}},
        {Memory.JUMP, new string[]{"You remembered how to jump.", "You've forgotten how to jump."}},
        {Memory.RUN, new string[]{"You remembered how to run.", "You've forgotten how to run."}},
        {Memory.ELEVATOR, new string[]{"You remembered the elevator code.", "You've forgotten the elevator code."}},
        {Memory.DOOR, new string[]{"You remembered the door combination.", "You've forgotten the door combination."}},
    };

    [SerializeField]
    private float startRememberTime = 10f;

    [SerializeField]
    private float rememberBuddyBoost = 10f;

    [SerializeField]
    private float imageFadeTime = 3;

    [SerializeField]
    private int imageFadeWaves = 3;

    [SerializeField]
    private float imageFadeInTime = 1.5f;

    [SerializeField]
    private int imageFadeInWaves = 2;

    [SerializeField]
    private float connectedPartForgetSpeed = 0.1f;

    [SerializeField]
    private float forgetSpeedLevelMultiplier = 1f;

    [SerializeField]
    internal RectTransform brainRoot;

    [SerializeField]
    private RawImage imageDoor;
    [SerializeField]
    private RawImage imageDoorConnector;
    [SerializeField]
    private RawImage imageElevator;
    [SerializeField]
    private RawImage imageJump;
    [SerializeField]
    private RawImage imageJumpConnector;
    [SerializeField]
    private RawImage imageName;
    [SerializeField]
    private RawImage imageNameConnector;
    [SerializeField]
    private RawImage imageRun;
    [SerializeField]
    private RawImage imageRunConnector;

    private Dictionary<MindPart, MindPart[]> mindMap = new Dictionary<MindPart, MindPart[]>();
    private Dictionary<MindPart, float> rememberTimes = new Dictionary<MindPart, float>();
    private Dictionary<MindPart, float> forgettingSpeed = new Dictionary<MindPart, float>();
    private Dictionary<MindPart, float> blinkTimes = new Dictionary<MindPart, float>();
    private Vector2 lastBrainPosition;

    public Vector2 position {
        get {
            Vector3 vp = Camera.main.WorldToViewportPoint(GameControl.instance.GetPlayer().position);
            if (vp.x > 0.65f) {
                return new Vector2(-750f, 335f);
            } else if (vp.x < 0.35f) {
                return new Vector2(745f, 335f);
            } else {
                return brainRoot.anchoredPosition;
            }
        }
    }

    void Awake() {
        // Build mind
        Remember(MINDPART_DAUGHTER, true);
        if (Application.isEditor) {
            // Remember(MINDPART_JUMP, true);
            // Remember(MINDPART_RUN, true);
            // Remember(MINDPART_RUN, true);
            // Remember(MINDPART_DOOR_COMBINATION, true);
            Remember(MINDPART_ELEVATOR, true);
            Remember(MINDPART_RUN, true);
            Remember(MINDPART_RUN_CONNECTOR, true);
            Remember(MINDPART_JUMP, true);
            Remember(MINDPART_JUMP_CONNECTOR, true);
            Remember(MINDPART_NAME_CONNECTOR, true);
            Remember(MINDPART_NAME, true);
            Remember(MINDPART_HINT_CLOSEDSTAIRWAY, true);
            Remember(MINDPART_HINT_DOOR, true);
        }

        // Build mind map
        mindMap[MINDPART_JUMP_CONNECTOR] = new MindPart[]{MINDPART_DAUGHTER, MINDPART_JUMP};
        mindMap[MINDPART_ELEVATOR] = new MindPart[]{MINDPART_NAME};
        mindMap[MINDPART_RUN_CONNECTOR] = new MindPart[]{MINDPART_DAUGHTER, MINDPART_RUN};
        mindMap[MINDPART_JUMP_CONNECTOR] = new MindPart[]{MINDPART_DAUGHTER, MINDPART_JUMP};
        mindMap[MINDPART_DOOR_CONNECTOR] = new MindPart[]{MINDPART_DAUGHTER, MINDPART_DOOR_COMBINATION};
        mindMap[MINDPART_NAME_CONNECTOR] = new MindPart[]{MINDPART_DAUGHTER, MINDPART_NAME};

        // Set images
        MINDPART_DOOR_COMBINATION.image = imageDoor;
        MINDPART_DOOR_CONNECTOR.image = imageDoorConnector;
        MINDPART_ELEVATOR.image = imageElevator;
        MINDPART_JUMP.image = imageJump;
        MINDPART_JUMP_CONNECTOR.image = imageJumpConnector;
        MINDPART_NAME.image = imageName;
        MINDPART_NAME_CONNECTOR.image = imageNameConnector;
        MINDPART_RUN.image = imageRun;
        MINDPART_RUN_CONNECTOR.image = imageRunConnector;
    }

    void Start() {
        lastBrainPosition = position;
    }

	// Update is called once per frame
	void Update () {
        if (GameControl.IsTalkMode() || GameControl.IsMenuMode())
            return;

        if (lastBrainPosition.x != position.x) {
            brainRoot.anchoredPosition = position;
            lastBrainPosition = position;
        }

        // get part from rememberTimes, lose it according to strength
        // blink before end
        Dictionary<MindPart, float> tmp = new Dictionary<MindPart, float>(rememberTimes);
        foreach (MindPart part in tmp.Keys) {
            if (part.unforgettable)
                continue;

            float t = rememberTimes[part];
            t -= GetForgettingSpeed(part) * Time.deltaTime;
            if (t <= 0) {
                Forget(part);
            } else {
                rememberTimes[part] = t;
            }

            ApplyMindPartImage(part);
        }
	}

    internal bool CanRemember(Memory memory) {
        foreach (MindPart part in rememberTimes.Keys)
            if (part.memory == memory)
                return true;
        return false;
    }

    internal bool CanRemember(MindPart mindPart) {
        foreach (MindPart part in rememberTimes.Keys)
            if (part.name == mindPart.name)
                return true;
        return false;
    }

    private void Forget(MindPart part) {
        rememberTimes.Remove(part);
        forgettingSpeed.Clear();

        // Play pong
        GameControl.instance.AddMessage(textsMap[part.memory][1], GameControl.MESSAGECOLOR_INFO, GameControl.SpeechType.SYSTEM_FORGET, false, part);
        //OneShotAudio.PlayClip(GameControl.instance.soundForget, Vector3.zero, 0f);

        ApplyMindPartImage(part);
    }

    public void Remember(string name, bool silent = false) {
        MindPart part = GetMindPart(name, true);
        if (part != null) {
            Remember(part);
        } else {
            Debug.LogError("Unknown memory: " + name);
        }
    }

    // public void Remember(Memory memory, bool silent = false) {
    //     MindPart part = GetMindPart(name, false);
    //     if (part != null) {
    //         Remember(part);
    //     } else {
    //         Debug.LogError("Unknown memory: " + memory);
    //     }
    // }

    public void Remember(MindPart mindPart, bool silent = false) {
        bool isNew = !rememberTimes.ContainsKey(mindPart);
        rememberTimes[mindPart] = startRememberTime;
        forgettingSpeed.Clear();

        // Add time to all directly connected parts
        if (mindMap.ContainsKey(mindPart)) {
            foreach (MindPart buddy in mindMap[mindPart]) {
                if (rememberTimes.ContainsKey(buddy))
                    rememberTimes[buddy] = Mathf.Min(rememberTimes[buddy] + rememberBuddyBoost, startRememberTime);
            }
        }

        // Play ping
        if (isNew && !silent) {
            GameControl.instance.AddMessage(textsMap[mindPart.memory][0], GameControl.MESSAGECOLOR_INFO, GameControl.SpeechType.SYSTEM_REMEMBER, false, mindPart);
            //OneShotAudio.PlayClip(GameControl.instance.soundRemember, Vector3.zero, 0f);
            blinkTimes[mindPart] = 0;
        }

        ApplyMindPartImage(mindPart);
    }

    private void ApplyMindPartImage(MindPart part) {
        if (part.image == null)
            return;
        bool active = rememberTimes.ContainsKey(part);
        float f = active ? rememberTimes[part] : 0;
        if (part.image.gameObject.activeSelf != active)
            part.image.gameObject.SetActive(active);
        if (f < imageFadeTime) {
            float r = Mathf.Pow((f / imageFadeTime), 0.6f) * (Mathf.PI * (2 * imageFadeWaves + 0.5f));
            part.image.color = Color.Lerp(Color.white, GameControl.COLOR_INVISIBLE_WHITE, Mathf.Abs(Mathf.Sin(r)));
        } else if (blinkTimes.ContainsKey(part)) {
            blinkTimes[part] = blinkTimes[part] + Time.deltaTime;
            f = Mathf.Min(1, blinkTimes[part] / imageFadeInTime);
            float r = f * (Mathf.PI * imageFadeInWaves * 2);
            part.image.color = Color.Lerp(Color.white, GameControl.COLOR_INVISIBLE_WHITE, Mathf.Abs(Mathf.Sin(r)));
            if (f == 1)
                blinkTimes.Remove(part);
        } else {
            part.image.color = Color.white;
        }
    }

    private float GetForgettingSpeed(MindPart part) {
        if (part.unforgettable)
            return 0;

        if (forgettingSpeed.ContainsKey(part))
            return forgettingSpeed[part];

        List<MindPart> connectedParts = new List<MindPart>();
        GetConnectedParts(connectedParts, part);

        // Connected to center?
        foreach (MindPart bud in connectedParts)
            if (bud.memory == Brain.Memory.BASE)
                return connectedPartForgetSpeed;

        float speed = forgetSpeedLevelMultiplier * (1+part.level);

        // reduce with every connected parts
        speed -= speed / 10 * connectedParts.Count;

        // some bonus for door combination
        if (part.memory == Memory.DOOR)
            speed *= 0.6f;

        forgettingSpeed[part] = speed;
        return speed;
    }

    private void GetConnectedParts(List<MindPart> connectedParts, MindPart part, bool onlyActive = true) {
        if (mindMap.ContainsKey(part)) {
            MindPart[] buddies = mindMap[part];
            AddBuddies(connectedParts, part, onlyActive, buddies);
        }
        foreach (MindPart key in mindMap.Keys) {
            if (onlyActive && !rememberTimes.ContainsKey(key))
                continue;
            MindPart[] buddies = mindMap[key];
            if (Array.Exists(buddies, delegate(MindPart other) {
                return other.name == part.name;
            })) {
                connectedParts.Add(key);
                AddBuddies(connectedParts, part, onlyActive, buddies);
            }
        }
    }

    private void AddBuddies(List<MindPart> connectedParts, MindPart part, bool onlyActive, MindPart[] buddies) {
        foreach (MindPart buddy in buddies) {
            if (part.name == buddy.name)
                continue;
            if (onlyActive && !rememberTimes.ContainsKey(buddy))
                continue;
            if (!connectedParts.Contains(buddy)) {
                connectedParts.Add(buddy);
                GetConnectedParts(connectedParts, buddy);
            }
        }
    }
}
