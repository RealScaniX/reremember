﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpecialEvent : MonoBehaviour {
    public abstract void Trigger();
    public abstract void DeTrigger();
}
