using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class DoorTransition : TransitionGameMode {

    [Header("DoorTransition")]
    [SerializeField]
    private SpriteRenderer doorImage;

    [SerializeField]
    private Sprite[] animTextures;

    [SerializeField]
    private bool byScript;

    internal override void FinishTransition() {
        if (transitionForward && byScript)
            return;
        Apply(transitionForward ? 1 : 0);
    }

    internal override void PlayTransition(float f) {
        if (transitionForward && byScript)
            return;
        Apply(transitionForward ? f : (1-f));
    }

    public void Apply(float f) {
        int frame = Mathf.FloorToInt(Mathf.Min(1, Mathf.Max(0, f)) * animTextures.Length);
        doorImage.sprite = animTextures[Math.Min(animTextures.Length-1, frame)];
    }

    internal override void PrepareTransition() {
        if (byScript)
            duration = transitionForward ? 0.01f : 0.75f;
   }
}