using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameMode : MonoBehaviour, GameMode {

    [SerializeField]
    private PlayerController player;

    public void EnterMode(bool push) {
    }

    public void FixedUpdateMode() {
    }

    public void LeaveMode(bool pop) {
    }

    public void UpdateMode() {
    }
}