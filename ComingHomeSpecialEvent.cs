﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ComingHomeSpecialEvent : SpecialEvent, GameMode {

    [SerializeField]
    public GameObject door;

    private float startTime;
    private int phase;
    private float destinationPosition;
    private float startPosition;
    private DoorTransition doorScript;

    private float phaseTime {
        get {
            return Time.time - startTime;
        }
    }

    public override void Trigger() {
        GameControl.instance.ClearStack();
        GameControl.instance.PushGameMode(this);
    }

    public override void DeTrigger() {
    }

    public void EnterMode(bool push) {
        startTime = Time.time;
        phase = 0;
        destinationPosition = door.transform.position.x;
        startPosition = GameControl.instance.GetPlayer().position.x;
        doorScript = door.GetComponent<DoorTransition>();
    }

    public void LeaveMode(bool pop) {
    }

    public void UpdateMode() {
        switch (phase) {
            case 0: {
                // Wait until dialog is over
                if (!GameControl.instance.HasPendingMessages()) {
                    Vector3 position = GameControl.instance.GetPlayer().transform.position;
                    position.z = 0.15f;
                    GameControl.instance.GetPlayer().transform.position = position;
                    AdvancePhase();
                }
                break;
            }

            case 1: {
                float f = Mathf.Min(1, phaseTime / 2.0f);
                Vector2 position = GameControl.instance.GetPlayer().position;
                position.x = Mathf.SmoothStep(startPosition, destinationPosition, f);
                GameControl.instance.GetPlayer().position = position;
                if (f == 1) {
                    AdvancePhase();
                }
                break;
            }

            case 2: {
                float f = Mathf.Min(1, phaseTime / 2.0f);
                doorScript.Apply(1-f);
                if (f == 1) {
                    GameControl.instance.AddMessage("Happy End!", new Color(0.2f, 1f, 0.3f), GameControl.SpeechType.SYSTEM_SILENT, true);
                    AdvancePhase();
                }
                break;
            }

            case 3: {
                // Wait until dialog is over
                if (!GameControl.instance.HasPendingMessages()) {
                    AdvancePhase();
                }
                break;
            }

            case 4: {
                float f = Mathf.Min(1, phaseTime / 2.0f);
                if (f == 1) {
                    AdvancePhase();
                }
                break;
            }

            default: {
                // End
                GameControl.gameStarted = false;
                SceneManager.LoadScene("game");
                break;
            }
        }
    }

    public void FixedUpdateMode() {
    }

    private void AdvancePhase() {
        phase++;
        startTime = Time.time;
    }
}
