﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallMover : MonoBehaviour {

    [SerializeField]
    private float delay;

    [SerializeField]
    private float delayFac = 0.5f;

    [SerializeField]
    private float offset;

    [SerializeField]
    private float speed;

    private Transform tx;
    private float startTime;
    private Vector3 startPos;

    // Use this for initialization
    void Start () {
        tx = transform;
        startTime = Time.unscaledTime;
        startPos = tx.position;
	}

	// Update is called once per frame
	void FixedUpdate () {
        // if player is too close, move to the right when closing

        float f = 0;
        Vector3 playerPos = GameControl.instance.GetPlayer().position;
        if (playerPos.y < 7f) {
            float bw = 0.55f;
            f = Mathf.Max(0, Mathf.Sin((Time.unscaledTime - startTime) * speed - delay * delayFac));
            if (f < bw && playerPos.y >= 2.25f && playerPos.y <= 6.5f) {
                if (Mathf.Abs(playerPos.x - startPos.x) < 0.9f) {
                    playerPos.x = Mathf.Max(playerPos.x, startPos.x + 0.9f * (bw-f) / bw);
                    GameControl.instance.GetPlayer().position = playerPos;
                    GameControl.instance.GetPlayer().body.MovePosition(playerPos);
                    GameControl.instance.GetPlayer().body.velocity = Vector2.zero;
                }
            }
        }


        tx.position = startPos + Vector3.up * f * offset;
	}
}
